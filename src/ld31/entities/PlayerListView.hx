package ld31.entities;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import ld31.model.Player;
import openfl.display.BitmapData;
import openfl.filters.DropShadowFilter;
import tools.image.Scale3XUpscaler;

/**
 * ...
 * @author Samuel Bouchet
 */
class PlayerListView extends Control
{
	var img(get, null):Control;
	var nickname:Label;
	var clickCallback:Player->Void;
	public var button:Button;
	public var player:Player;
	public var isMainPlayer:Bool = false;

	public function new(x:Float=0, y:Float=0, clickCallback:Player->Void) 
	{
		super(x, y);
		this.clickCallback = clickCallback;

		img = new Control();
		addControl(img);
		
		nickname = new Label("", 0, -25);
		nickname.color = 0xC2D2FE;
		nickname.size = 32;
		nickname.textField.filters = [new DropShadowFilter(2,45,0x00366C,1,0,0,1,2,false),new DropShadowFilter(2,-135,0x00366C,1,0,0,1,1,false)];
		addControl(nickname);
		
		if (clickCallback != null) {
			button = new Button("", -10, -30, 80, 110);
			button.addEventListener(Button.CLICKED, function(e) {
				if (clickCallback != null) {
					clickCallback(player);
				}
			});
			addControl(button);
		}
		
	}
	
	public function viewPlayer(player:Player) {
		this.player = player;
		
		var imgData = player != null ? player.getImage() : null;
		if (player != null && imgData != null) {
			
			var bitmapData:BitmapData = Scale3XUpscaler.upscale(Scale3XUpscaler.upscale(imgData));
			img.graphic = new Image(bitmapData);
			if (isMainPlayer) {
				cast(img.graphic, Image).scale = 2;
			}

			nickname.text = player.name;
			var graphicCenter = isMainPlayer ? bitmapData.width : bitmapData.width / 2;
			nickname.localX = graphicCenter - nickname.width / 2;
			
		} else {
			img.graphic = null;
			nickname.text = "";
		}

	}
	
	function get_img():Control 
	{
		return img;
	}
	
}
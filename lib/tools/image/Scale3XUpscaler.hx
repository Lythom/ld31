package tools.image;
import openfl.display.BitmapData;

/**
 * ...
 * @author Samuel Bouchet
 */
class Scale3XUpscaler
{

    /**
     * Thanks to http://scale2x.sourceforge.net/algorithm.html.
     * @param   input
     * @return
     */
    public static function upscale(input:BitmapData):BitmapData {
        var output:BitmapData = new BitmapData( input.width*3, input.height*3, true );
  
        var E:UInt;
        
        var E0:UInt;
        var E1:UInt;
        var E2:UInt;
        var E3:UInt;
        var E4:UInt;
        var E5:UInt;
        var E6:UInt;
        var E7:UInt;
        var E8:UInt;
        
        var A:UInt;
        var B:UInt;
        var C:UInt;
        var D:UInt;
        var F:UInt;
        var G:UInt;
        var H:UInt;
        var I:UInt;
        
        var x:Int;
        var y:Int;
        
        var i3:Int;
        var j3:Int;
        
        var w:Int = input.width;
        var h:Int = input.height;
        
        input.lock();
        output.lock();
        for (x in 0...w) {
            for (y in 0...h) {
				
				// INPUT
				// 	A	B	C
				// 	D	E	F
				// 	G	H	I
                A = (x-1 < 0 || y - 1 < 0) 		? 0 : input.getPixel32( cast Math.max(0,x-1)  , cast Math.max(0,y-1) );
                B = (y-1 < 0)			 		? 0 : input.getPixel32( x                 , cast Math.max(0,y-1) );
                C = (x + 1 > w || y - 1 < 0) 	? 0 : input.getPixel32( cast Math.min(w, x + 1)  , cast Math.max(0, y - 1) );
                D = (x-1 < 0) 					? 0 : input.getPixel32( cast Math.max(0,x-1)  , y );
                E = (y-1 < 0) 					? 0 : input.getPixel32( x                 , y );
                F = (x+1 > w) 					? 0 : input.getPixel32( cast Math.min(w,x+1)  , y );
                G = (x-1 < 0 || y + 1 > h) 		? 0 : input.getPixel32( cast Math.max(0,x-1)  , cast Math.min(h,y+1) );
                H = (y + 1 > h) 				? 0 : input.getPixel32( x                 , cast Math.min(h,y+1) );
                I = (x+1 > w || y + 1 > h) 		? 0 : input.getPixel32( cast Math.min(w,x+1)  , cast Math.min(h,y+1) );
                    
                if ( diff(B,H) && diff(D,F) ) 
                {
                    // OUTPUT
					// 	E0	E1	E2
					// 	E3	E4	E5
					// 	E6	E7	E8
                    E0 = ( !diff(D,B) ) ? D : E;
                    E1 = ( ( !diff(D,B) && diff(E,C) ) || ( !diff(B,F) && diff(E,A) ) ) ? B : E;
                    E2 = ( !diff(B,F) )? F : E;
                    E3 = ( ( !diff(D,B) && diff(E,G) ) || ( !diff(D,H) && diff(E,A) ) ) ? D : E;
                    E4 = E;
                    E5 = ( ( !diff(B,F) && diff(E,I) ) || ( !diff(H,F) && diff(E,C) ) ) ? F : E;
                    E6 = ( !diff(D,H) ) ? D : E;
                    E7 = ( ( !diff(D,H) && diff(E,I) ) || ( !diff(H,F) && diff(E,G) ) ) ? H : E;
                    E8 = ( !diff(H,F) ) ? F : E;
                    
                } else {
                    
                    E0 = E;
                    E1 = E;
                    E2 = E;
                    E3 = E;
                    E4 = E;
                    E5 = E;
                    E6 = E;
                    E7 = E;
                    E8 = E;
                }

                i3 = x*3;
                j3 = y*3;
                
                output.setPixel32(   i3, j3, E0 );
                output.setPixel32( i3+1, j3, E1 );
                output.setPixel32( i3+2, j3, E2 );
                
                output.setPixel32(   i3, j3+1, E3 );
                output.setPixel32( i3+1, j3+1, E4 );
                output.setPixel32( i3+2, j3+1, E5 );
                
                output.setPixel32(   i3, j3+2, E6 );
                output.setPixel32( i3+1, j3+2, E7 );
                output.setPixel32( i3+2, j3+2, E8 );
                
            }
        }
        input.unlock();
        output.unlock();
        return output;
    }
    
    static function rgb2yuv(c:UInt):UInt
    {
        var r, g, b, y, u, v:UInt;

        r = (c & 0xFF0000) >> 16;
        g = (c & 0x00FF00) >> 8;
        b = c & 0x0000FF;
        y = cast (0.299*r + 0.587*g + 0.114*b);
        u = cast (-0.169*r - 0.331*g + 0.5*b) + 128;
        v = cast (0.5*r - 0.419*g - 0.081*b) + 128;

        return (y << 16) + (u << 8) + v;
    }
    
    /**
     * Indicate if the colors are different enough (RGB)
     * @param   rgb1
     * @param   rgb2
     * @return
     */
    static function diff(rgb1:UInt, rgb2:UInt):Bool {
		var a1 = rgb1 >> 24;
		var a2 = rgb2 >> 24;
        return (Math.abs(a1 - a2) > 0.2) || wdiff(rgb2yuv(rgb1),rgb2yuv(rgb2));
    }
    
    /**
     * Indicate if the colors are different enough (YUV)
     * @param   yuv1
     * @param   yuv2
     * @return
     */
    static function wdiff(yuv1:UInt, yuv2:UInt):Bool {

        var YMASK = 0xff0000;
        var UMASK = 0x00ff00;
        var VMASK = 0x0000ff;
        return Math.abs((yuv1 & YMASK) - (yuv2 & YMASK)) > ( 48 << 16) ||
               Math.abs((yuv1 & UMASK) - (yuv2 & UMASK)) > ( 7 <<  8) ||
               Math.abs((yuv1 & VMASK) - (yuv2 & VMASK)) > ( 8 <<  0);
    }
    
}
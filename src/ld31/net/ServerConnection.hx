package ld31.net ;
import haxe.Serializer;
import haxe.Unserializer;
import ld31.net.NetMessage.NetMessageType;
import openfl.Assets;
import openfl.errors.Error;
import openfl.events.Event;
import openfl.events.IOErrorEvent;
import openfl.events.ProgressEvent;
import openfl.events.SecurityErrorEvent;
import openfl.Lib;
import openfl.net.Socket;
import openfl.utils.ByteArray;

/**
 * ...
 * @author Samuel Bouchet
 */
class ServerConnection
{
	static private var socket:Socket;

	public function new() 
	{
		
	}
	
	public static function initNetwork()
	{

		socket = new Socket();
		socket.addEventListener(Event.CONNECT, connectHandler); 
		socket.addEventListener(Event.CLOSE, closeHandler); 
		socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, closeHandler); 
		socket.addEventListener(IOErrorEvent.IO_ERROR, closeHandler);
		socket.addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler); 
	//#if debug
		//socket.connect("localhost", 13337);
	//#else
		socket.connect("samuel-bouchet.fr", 13337);
	//#end
		
	}
	
	static private function closeHandler(e:Dynamic):Void 
	{
		trace(e);
		CockpitScene.instance.disconnect();
	}
	
	static public function sendWelcome() {
		var shipname:String = Lib.current.loaderInfo.parameters.name;
		if (shipname == null || shipname == "") {
			shipname = "debug1337";
		}
		socket.writeUTFBytes(Serializer.run(new NetMessage(NetMessageType.Welcome, Serializer.run(shipname))));
		socket.flush();
	}
	
	static public function sendStartTrip(planetid:Int) 
	{
		socket.writeUTFBytes(Serializer.run(new NetMessage(NetMessageType.StartTrip, Serializer.run(planetid))));
		socket.flush();
	}
	
	static public function sendCancelTrip(planetid:Int) 
	{
		socket.writeUTFBytes(Serializer.run(new NetMessage(NetMessageType.CancelTrip, Serializer.run(planetid))));
		socket.flush();
	}
	
	static public function sendTryLetter(text:String) 
	{
		socket.writeUTFBytes(Serializer.run(new NetMessage(NetMessageType.TryLetter, Serializer.run(text))));
		socket.flush();
	}
	
	static private function connectHandler(e:Event):Void 
	{
		sendWelcome();
	}
	
	static var fromServer:String    = "";
	private static function socketDataHandler(e:ProgressEvent):Void 
	{
		fromServer += socket.readUTFBytes(socket.bytesAvailable);

		// it's a full message, create your object, then clear fromServer
		var message:NetMessage = null;
		try 
		{
			message = Unserializer.run(fromServer);
		} catch (err:Dynamic)
		{
			trace(err);
			return;
		}
		
		if (message == null) {
			return;
		}
		if (message.data == null) {
			message.data = { };
		}
		
		// it's server answer or message
		switch(message.type) {
			case NetMessageType.Welcome:
				CockpitScene.instance.welcome(message.data);

			case NetMessageType.StartTrip :
				// Trip ACK
				CockpitScene.instance.startTripConfirmation(message.data);
				
			case NetMessageType.CancelTrip :
				// Cancel Trip ACK
				CockpitScene.instance.cancelTripConfirmation(message.data);
				
			case NetMessageType.Goto :
				// change planet
				CockpitScene.instance.goto(message.data);
				
			case NetMessageType.PlayerChange :
				// change planet
				CockpitScene.instance.playerChange(message.data);
			
			case NetMessageType.TripChange:
				// player joined or left the trip
				CockpitScene.instance.tripChange(message.data);
				
			case NetMessageType.TryLetter:
				// try letter result
				CockpitScene.instance.letterResult(Std.is(message.data,String) ? message.data : null);
				
		}
		
		fromServer = "";
	}
	
}
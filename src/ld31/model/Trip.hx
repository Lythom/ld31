package ld31.model;

/**
 * @author Samuel Bouchet
 */

typedef Trip =
{
	from:Int, // planet id
	to:Int,   // planet id
	with :Array<Int>, // players id
	starttime:Float, // timestamp at wich the trip started (servertime)
	cost:Int // cost in power
}
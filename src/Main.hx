import com.haxepunk.Engine;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.CheckBox;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import ld31.CockpitScene;

#if flash
import org.flashdevelop.utils.FlashConnect;
#end

class Main extends Engine
{
	public static inline var kScreenWidth:Int = 1280;
	public static inline var kScreenHeight:Int = 800;
	public static inline var kFrameRate:Int = 60;
	public static inline var kClearColor:Int = 0x333333;
	public static inline var kProjectName:String = "HaxePunk";

	public function new()
	{
		super(kScreenWidth, kScreenHeight, kFrameRate, true);
	}

	override public function init()
	{
#if debug
	#if flash
		if (flash.system.Capabilities.isDebugger)
	#end
		{
			HXP.console.enable();
			HXP.console.toggleKey = Key.P;
		}
#else
		FlashConnect.redirect();
#end
		
		initHaxepunkGui();
		
		HXP.screen.color = kClearColor;
		HXP.screen.scale = 1;
		HXP.scene = new CockpitScene();
		
		Input.define("up", [Key.Z, Key.W, Key.UP]);
		Input.define("right", [Key.D, Key.RIGHT]);
		Input.define("down", [Key.S, Key.DOWN]);
		Input.define("left", [Key.Q, Key.A, Key.LEFT]);
		Input.define("action1", [Key.E, Key.ENTER]);
	}

	public static function main()
	{
		new Main();
	}
	
	override public function update()
	{
		super.update();
	}
	
		
	private function initHaxepunkGui():Void 
	{
		// Choose custom skin. Parameter can be a String to resource or a bitmapData.
		Control.useSkin("gfx/ui/ld31ui.png");
		// The default layer where every component will be displayed on.
		// Most components use severals layers (at least 1 per component child). A child component layer will be <100.
		Control.defaultLayer = 100;
		// Use this to fit your button skin's borders, set the default padding of every new Button and ToggleButton.
		// padding attribute can be changed on instances after creation.
		Button.defaultPadding = 4;
		// Size in px of the tickBox for CheckBoxes. Default is skin native size : 12.
		CheckBox.defaultBoxSize = 12;
		// Same for RadioButtons.
		RadioButton.defaultBoxSize = 12;
		// Label defaults parameters affect every components that uses labels : Button, ToggleButton, CheckBox, RadioButton, MenuItem, Window Title.
		// Those labels are always accessible using "myComponent.label" and you can change specific Labels apperence any time.
		// Label default font (must be a flash.text.Font object).
		Label.defaultFont = openfl.Assets.getFont("font/lythgame.ttf");
		// Label defaultColor. Tip inFlashDevelop : use ctrl + shift + k to pick a color.
		Label.defaultColor = 0x004A7F;
		// Label default Size.
		Label.defaultSize = 32;
	}

}
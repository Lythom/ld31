package ld31.model;

/**
 * @author Samuel Bouchet
 */

typedef PlanetData =
{
	var players:Array<Player>;
	var planets:Array<Planet>;
	var thisplanet:Planet;
	var thisplayer:Player;
}
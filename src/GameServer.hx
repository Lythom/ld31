package ;
import haxe.ds.HashMap;
import haxe.ds.IntMap;
import haxe.io.Bytes;
import haxe.Serializer;
import haxe.Timer;
import haxe.Unserializer;
import ld31.model.Planet;
import ld31.model.PlanetData;
import ld31.model.Player;
import ld31.model.PlayerChangeData;
import ld31.model.Trip;
import ld31.model.TripChangeData;
import ld31.net.NetMessage;
import ld31.procedural.NameGenerator;
import ld31.procedural.SpriteGenerator;
import neko.Lib;
import neko.net.ThreadServer;
import openfl.display.BitmapData;
import openfl.errors.Error;
import psg.Mask;
import sys.db.Connection;
import sys.db.Mysql;
import sys.db.ResultSet;
import sys.io.File;
import sys.net.Socket;


/**
 * ...
 * @author Samuel Bouchet
 */
typedef Client = {
  var id : Int;
  var validated : Bool;
  var socket:Socket;
  var player:Player;
}

typedef Message = {
  var type : NetMessageType;
  var data : Dynamic;
}
	
class GameServer extends ThreadServer<Client, Message>
{
	public function new() { 
		super();
	}
	
	static var clientNumber:Int = 0;
	
	static var shipTemplate:BitmapData = null;
	static var popTemplate:BitmapData = null;
	static var planetTemplate:BitmapData = null;
	
	var currentTrips:Array<Trip> = new Array<Trip>();
	var clients:IntMap<Client> = new IntMap<Client>();
	
	var populationMask = new Mask([
			0, 0, 0, 0,
			0, 1, 1, 1,
			0, 1, 2, 2,
			0, 0, 1, 2,
			0, 0, 0, 2,
			1, 1, 1, 2,
			0, 1, 1, 2,
			0, 0, 0, 2,
			0, 0, 0, 2,
			0, 1, 2, 2,
			1, 1, 0, 0
	], 4, 11, true, false);
	
	var planetMask = new Mask([
			0, 0, 0, 2, 2, 2, 2, 0, 0, 0,
			0, 0, 2, 1, 1, 1, 1, 2, 0, 0,
			0, 2, 1, 1, 1, 1, 1, 1, 2, 0,
			2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
			2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
			2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
			2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
			0, 2, 1, 1, 1, 1, 1, 1, 2, 0,
			0, 0, 2, 1, 1, 1, 1, 2, 0, 0,
			0, 0, 0, 2, 2, 2, 2, 0, 0, 0
	], 10, 10, false, false, 1);
	
	var shipMask = new Mask([
			0, 0, 0, 2,
			0, 0, 0, 2,
			0, 0, 2, 1,
			0, 0, 1, 1,
			0, 1, 1, 1,
			0, 1, 1, 1,
			0, 0, 1, 2,

	], 4, 7, true, false,1);
	
	override function init() 
	{
		super.init();
		
		// TODO : par sécurité, passer tous les jouers à online = 0 au démarrage.
	}
	
	override public function update() 
	{
		// update loop every second
		var stamp:Float = Timer.stamp();
		
		// check trips to be started
		var startedTrips = checkTripStarts(stamp, this.currentTrips);
		// started trip doesn't need to be remembered
		for (trip in startedTrips) {
			currentTrips.remove(trip);
		}
		
		// update player fuel
		var cnx = getMysqlConnexion();
		cnx.startTransaction();
		cnx.request("UPDATE player SET fuel = LEAST(100,fuel + 1)");
		cnx.commit();
		cnx.close();
	}
	
	function checkTripStarts(stamp:Float, currentTrips:Array<Trip>):Array<Trip> 
	{
		
		var startedTrips = [];
		for (trip in currentTrips) {
			
			// TODO : count player and reduce jump time if alone.
			
			if (stamp - trip.starttime  > 10) {
				// trip starts !
				Lib.println("trip starts " + trip);
				startedTrips.push(trip);
				var totalFuel:Int = 0;
				for (playerid in trip.with ) {
					var client = clients.get(playerid);
					if (client != null && client.player != null) {
						totalFuel += client.player.fuel;
					}
				}
				var playerlist = Lambda.array(Lambda.map(trip.with , function(playerid:Int) { return clients.exists(playerid) ? clients.get(playerid).player : null ; } ));
				// trip has started, indicate other players leaving
				if (totalFuel >= trip.cost) {
					notifyOtherPlayersOnPlanet({
						playernames: playerlist,
						arrived: false,
						left: true
					});
				}
				// send each player the goto or the cancel
				var went:Bool = false;
				for (playerid in trip.with ) {
					var client = clients.get(playerid);
					if (client != null && client.socket != null) {
						if (totalFuel >= trip.cost) {
							goto(client, trip);
							went = true;
						} else {
							sendTripCancel(client, true);
						}
					}
				}
				
				// trip has arrived, indicate other players arriving
				if (went) {
					notifyOtherPlayersOnPlanet({
						playernames: playerlist,
						arrived: true,
						left: false
					});
				}
			}
		}
		return startedTrips;
	}

	// create a Client
	override function clientConnected( s : Socket ) : Client
	{
		var num = ++ clientNumber;
		Lib.println("connexion " + s.peer() +". Users : "+num);
		return { id: num, validated: false, socket: s, player:null };
	}

	override function clientDisconnected( c : Client )
	{
		var num = -- clientNumber;
		Lib.println("client " + Std.string(c.id) + " disconnected. Users : " + num);
		var cnx = getMysqlConnexion();
		cnx.request("UPDATE player SET online = 0 WHERE id = "+c.id);
		cnx.close();
		
		notifyOtherPlayersOnPlanet( {
			playernames: ((c.player != null) ? [c.player] : []),
			arrived: false,
			left: true
		});
	}

	override function readClientMessage(c:Client, buf:Bytes, pos:Int, len:Int)
	{
		var messageStr = buf.getString(pos, len);
		var message:NetMessage = null;
		if (messageStr.length > 0 && messageStr.charAt(0) == "<") {
			c.socket.output.writeString("<cross-domain-policy><site-control permitted-cross-domain-policies=\"master-only\"/><allow-access-from domain=\"*\" to-ports=\"*\" /></cross-domain-policy>"+String.fromCharCode(0));
			c.socket.output.flush();
			return {msg: null, bytes: len};
		} else {
			message = Unserializer.run(messageStr);
			return {msg: {type:message.type ,data:message.data}, bytes: len};
		}
		
		//var end:Int = bytesIn.readByte();
		
	}
	
	override function clientMessage( c : Client, msg : Message )
	{
		if (msg == null) return;
		switch (msg.type) 
		{
			case NetMessageType.Welcome :
				sendWelcome(c, msg);
				Lib.println(c.id + " answerWelcome(c)");

			case NetMessageType.StartTrip :
				// Trip ACK
				var sending:String = Serializer.run(new NetMessage(NetMessageType.StartTrip, startTrip(c, msg)));
				c.socket.output.writeString(sending);
				c.socket.output.flush();
				Lib.println("start trip sent to " + c.id + " : " + sending);
				
			case NetMessageType.CancelTrip :
				// Cancel Trip ACK
				cancelTrip(c, msg);
				
			case NetMessageType.Goto :
				// Should not append, Server -> Client only
				
			case NetMessageType.PlayerChange :
				// change planet
				// Should not append, Server -> Client only

			case NetMessageType.TripChange :
				// change planet
				// Should not append, Server -> Client only
			
			case NetMessageType.TryLetter :	
				tryLetter(c,msg);
		}
	}
	
	function tryLetter(c:Client, msg:Message) 
	{
		var letter:String = Unserializer.run(msg.data);
		// TODO : check letter validity
		// no time, assume the letter allowed for this player
		trace(letter);
		if (letter == "â˜ƒ" || "abcdefghijklmnopqrstuvwxyz".indexOf(letter.toLowerCase()) > -1) {
			// get the world phrase
			var cnx = this.getMysqlConnexion();
			cnx.startTransaction();

			var rset = cnx.request("SELECT planet.id as planetid, planet.popphrasefinale, planet.popphrasecurrent FROM planet JOIN player ON player.position_id = planet.id WHERE player.id = " + c.id);
			
			var found:String = null;
			var planetid:Int = -1;
			for ( row in rset ) {
				
				planetid = row.planetid;
				
				var answer:String = row.popphrasefinale;
				var current:String = row.popphrasecurrent;
				for (i in 0...answer.length) {
					if (answer.charAt(i).toLowerCase() == letter.toLowerCase() || answer.charAt(i) == letter) {
						if (current.charAt(i) == ".") {
							// we got a match
							current = current.substring(0, i) + answer.charAt(i) + current.substring(i + 1);
							cnx.request("UPDATE planet SET popphrasecurrent = " + cnx.quote(current) + " WHERE id = " + row.planetid);
							
							found = current;
							break;
						}
					}
				}
				
				if (found == null) {
					cnx.request("UPDATE player_planet SET triescount = triescount + 1 WHERE id_planet = " + row.planetid + " AND id_player = " + c.id);
				} else {
					cnx.request("UPDATE player SET reputation = reputation + 1 WHERE online = 1 AND position_id = " + row.planetid + " AND INSTR(skills," + cnx.quote(letter.toUpperCase()) +")");
					cnx.request("UPDATE player SET reputation = reputation + 1 WHERE id = " + c.id);
				}
				
				var sending:String = Serializer.run(new NetMessage(NetMessageType.TryLetter, found));
				c.socket.output.writeString(sending);
				c.socket.output.flush();
			
			}
			
			cnx.commit();
			cnx.close();
			
			notifyOtherPlayersOnPlanet(null,{
				players : [found],
				destination: -1,
				from: planetid
			});
		}
	}
	
	/**
	 * When a start trip is asked
	 * @param	c
	 * @param	msg
	 * @return
	 */
	function startTrip(c:Client, msg:Message):String
	{
		var result:String = "";
		var cnx = getMysqlConnexion();
		try {
			
			var rset:ResultSet = cnx.request("SELECT position_id, fuel FROM player WHERE player.id = " + c.id);
			var from:Int;
			var playerfuel:Int;
			for ( row in rset ) {
				from = row.position_id;
				// refresh fuel data
				c.player.fuel = row.fuel;
			}
			var to:Int = Std.int(Unserializer.run(msg.data)); // dest wished by player
			
			// calculate cost
			var distance:Int = -1;
			var rset:ResultSet = cnx.request("SELECT distance FROM distances WHERE id_planet1 = " + from + " AND id_planet2 = " + to);
			for ( row in rset ) {
				distance = row.distance;
			}
			
			var currTrip:Trip = {
				from: from,
				to:to,
				with : [c.id],
				cost : distance,
				starttime : Timer.stamp()
			};
			
			// register the player in an existing trip or register the new trip
			var isnewtrip = true;
			for (trip in currentTrips) {
				if (currTrip.from == trip.from && currTrip.to == trip.to) {
					trip.with.push(c.id);
					currTrip = trip;
					isnewtrip = false;
					Lib.println("trip joined registered "+currTrip);
					break;
				}
			}
			if (isnewtrip) {
				Lib.println("Adding trip to the stack "+currTrip);
				currentTrips.push(currTrip);
			}
			
			result = (isnewtrip ? "new" : "join");
			
			// notifie un début de voyage
			notifyOtherPlayersOnPlanet(null, {
				from: currTrip.from,
				destination: currTrip.to,
				players: [c.player.name]
			});
			
		} catch (err:Dynamic)
		{
			Lib.println(err);
			cnx.close();
			return "false";
		}
		cnx.close();
		return result;
	}
	
	/**
	 * Ask to cancel trip.
	 * @param	c
	 * @param	msg
	 * @return
	 */
	function cancelTrip(c:Client, msg:Message):Bool
	{
		var removedFromTrip:Trip = null;
		for (trip in currentTrips) {
			// find the trip with the player
			if (Lambda.has(trip.with , c.id)) {
				// remove player from trip
				trip.with.remove(c.id);
				removedFromTrip = trip;
				Lib.println(c.id + " removed from trip " + trip);
				break;
			}
		}
		// no one left, cancel the trip totally
		if (removedFromTrip != null && removedFromTrip.with .length == 0) {
			currentTrips.remove(removedFromTrip);
			// the trip doesn't exists anymore
			Lib.println("trip removed " + removedFromTrip);
		}
		
		// notify the player the cancel is ok
		sendTripCancel(c, true);

		// notify the player left the trip
		notifyOtherPlayersOnPlanet(null, {
			from : removedFromTrip.from,
			destination: removedFromTrip.to,
			players: Lambda.array(Lambda.map(removedFromTrip.with , function(playerid:Int) { return clients.exists(playerid) ? clients.get(playerid).player.name : "" ; } ))
		});
		
		return (removedFromTrip != null); //  true if the trip is cancelled, false if the trip has already left.
	}
	
	/**
	 * Send CancelTrip notif.
	 * @param	client
	 * @param	value
	 */
	function sendTripCancel(client:Client, value:Bool) 
	{
		try 
		{
			if (client != null && client.socket != null) {
				var sending:String = Serializer.run(new NetMessage(NetMessageType.CancelTrip, value));
				client.socket.output.writeString(sending);
				client.socket.output.flush();
			}
		} catch (e:Dynamic) {
			trace(e);
		}
		
	}
	
	
	/**
	 * Do move player
	 */
	function goto(client:Client, trip:Trip) 
	{
		if (client != null && client.player != null && client.player.name != null) {
			
			var destination:Int = trip.to;
			var cost:Int = Std.int(trip.cost / (trip.with).length);

			if (destination > 0) {
				var cnx = getMysqlConnexion();
				cnx.startTransaction();
				cnx.request("UPDATE player SET fuel = GREATEST(fuel - (" + cost + "), 0), position_id = " + destination + " WHERE id = " + client.id);
				cnx.commit();
				cnx.close();
				sendPlayerdata(client, client.player.name, NetMessageType.Goto);
			} else {
				sendTripCancel(client,true);
			}
		}
	}
	
	/**
	 * Send world data.
	 */
	function sendWelcome(c:Client, msg :Message) 
	{
		var playerlogin:String = Std.string(Unserializer.run(msg.data));
		var player:Player = sendPlayerdata(c, playerlogin, NetMessageType.Welcome);
		if (player != null) {
			notifyOtherPlayersOnPlanet( {
				playernames: [player],
				arrived: true,
				left: false
			});
		}
	}
	
	/**
	 * 
	 * @param	c
	 * @param	playerlogin
	 * @param	mtype
	 */
	function sendPlayerdata(c:Client, playerlogin:String, mtype:NetMessageType):Player
	{
		var player:Player = new Player();
		var planet:Planet = new Planet();
		
		// Check the player exists
		var cnx = getMysqlConnexion();
		cnx.startTransaction();
		
		var PLAYER_STATUS_QUERY = "SELECT player.id, player.login, player.name, player.img, player.reputation, player.fuel, player.skills, player.position_id," +
								  "planet.name as planetname, planet.posx as planetposx, planet.posy as planetposy, planet.img as planetimg, planet.popimg as planetpopimg, planet.popphrasecurrent as planetpopphrasecurrent, " +
								  "player_planet.triescount " +
								  "FROM player " +
								  "JOIN planet ON planet.id = player.position_id " +
								  "LEFT JOIN player_planet ON player_planet.id_planet = planet.id AND player_planet.id_player = player.id " +
								  "WHERE login = " + cnx.quote(playerlogin);
		
		var rset:ResultSet = cnx.request(PLAYER_STATUS_QUERY);
		
		// the player doesn't exists, create the account
		if (rset.length == 0) {
			var playerShip:BitmapData = SpriteGenerator.generateSprite(shipMask);
			cnx.request("INSERT INTO player VALUES (NULL,"+cnx.quote(playerlogin)+","+cnx.quote(playerlogin)+","+cnx.quote(Serializer.run(playerShip.getVector(playerShip.rect)))+",0,100,'..........................',1,1)");
			rset = cnx.request(PLAYER_STATUS_QUERY);
		}

		var planetid:Int ;
		// get the data of the player + the current planet
		for ( row in rset ) {
			
			if (row.triescount == null) {
				cnx.request("INSERT INTO player_planet VALUE (" + cnx.quote(Std.string(row.position_id)) + ", " + cnx.quote(Std.string(row.id)) + ", 0)");
				// earn a new letter
				var iLetter = Math.floor(Math.random() * 26);
				row.skills = Std.string(row.skills).substring(0, iLetter) + String.fromCharCode(iLetter + 65) + Std.string(row.skills).substring(iLetter + 1);
				cnx.request("UPDATE player SET skills = "+cnx.quote(row.skills)+" WHERE id = "+cnx.quote(Std.string(row.id)));
			}
			
			// create planet visuals if they don't exists
			if (row.planetimg == null || row.planetpopimg == null || row.planetname == "Undiscovered planet") {
				var planetimg:BitmapData = SpriteGenerator.generateSprite(planetMask);
				var planetpopimg:BitmapData = SpriteGenerator.generateSprite(populationMask);
				var planetname = NameGenerator.generatePlanetName();
				
				cnx.request("UPDATE planet SET img = " + cnx.quote(Serializer.run(planetimg.getVector(planetimg.rect))) +
						  ", popimg = " + cnx.quote(Serializer.run(planetpopimg.getVector(planetpopimg.rect))) +
						  ", name = " + cnx.quote(planetname) +
						  " WHERE id = " + Std.string(row.position_id) );
						  
				row.planetimg = { b: Serializer.run(planetimg.getVector(planetimg.rect)) };
				row.planetpopimg = { b: Serializer.run(planetpopimg.getVector(planetpopimg.rect)) };
				row.planetname = planetname;
			}
			
			planetid = row.position_id;
			
			// data to remember
			c.id = row.id;
			c.player = player;

			player.fuel = row.fuel;
			player.img = readImageFromRow(row.img);
			player.name = playerlogin;
			player.reputation = row.reputation;
			player.skills = row.skills;
			
			planet.id = planetid;
			planet.name = row.planetname;
			planet.img = readImageFromRow(row.planetimg);
			planet.popimg  = readImageFromRow(row.planetpopimg);
			planet.popphrasecurrent = row.planetpopphrasecurrent;
			planet.posx = row.planetposx;
			planet.posy = row.planetposy;
			planet.triescount = row.triescount;
			planet.distance = 0;
		}
		
		// get the data of reachable planets
		var planets:Array<Planet> = new Array<Planet>();
		rset = cnx.request("SELECT id_planet2, planet.name, planet.posx, planet.posy, planet.img, popphrasecurrent,  distance, (select count(id) FROM player WHERE player.position_id = id_planet2 AND player.online = 1) as playercount " +
		                   "FROM distances JOIN planet ON distances.id_planet2 = planet.id WHERE id_planet1 = " + planetid + " ORDER BY distance");
		
		for ( row in rset ) {
			var p:Planet = new Planet();
			p.id = row.id_planet2;
			p.name = row.name;
			p.distance = row.distance;
			p.playercount = row.playercount;
			p.posx = row.posx;
			p.posy = row.posy;
			p.popphrasecurrent = row.popphrasecurrent;
			p.img = readImageFromRow(row.img);
			
			planets.push(p);
		}
		
		// get the data of other connected players on the planet
		var players = getPlayersOnPlanet(planetid, cnx, player.name);
		planet.playercount = players.length + 1;
		
		cnx.request("UPDATE player SET online = 1 WHERE login = " + cnx.quote(playerlogin));

        cnx.commit();
        cnx.close();
		
		// data to register
		clients.set(c.id, c); // playerid => Client data
		
		// data to send to the client
		var gameData:PlanetData = {
			thisplayer : player,
			thisplanet : planet,
			players : players,
			planets : planets
		};
		
		var sending:String = Serializer.run(new NetMessage(mtype, gameData));
		c.socket.output.writeString(sending);
		c.socket.output.flush();
		
		return player;
	}
	
	function notifyOtherPlayersOnPlanet(playerChange:PlayerChangeData = null, tripChange:TripChangeData = null):Void {
		
		Lib.println("notifying " + playerChange + ", " + tripChange);
		
		var cnx = getMysqlConnexion();
		
		// get world
		var planetid:Int = 0;
		var rset;
		if (playerChange != null && playerChange.playernames != null && playerChange.playernames.length > 0) {
			rset = cnx.request("SELECT position_id FROM player WHERE login = " + cnx.quote(Std.string(playerChange.playernames[0].name)));
			for ( row in rset ) {
				planetid = row.position_id;
			}
			Lib.println("notifying 1 : "+ planetid);
		} else if (tripChange != null) {
			planetid = tripChange.from;
			Lib.println("notifying 2 : "+ planetid);
		}
		if (planetid == 0) {
			return;	
		}
		
		// send message to players from planet
		rset = cnx.request("SELECT id FROM player WHERE online = 1 AND position_id = " + planetid);
		cnx.close();

		var playerList = new Array<Int>();
		
		for ( row in rset ) {
			var sending:String = "";
			// find client and send him the right message
			var client:Client = clients.get(row.id);
			if (client != null && client.socket != null) {
				if (playerChange != null) {
					sending = Serializer.run(new NetMessage(NetMessageType.PlayerChange, playerChange ));

				} else if (tripChange != null) {
					sending = Serializer.run(new NetMessage(NetMessageType.TripChange, tripChange ));
				}
				Lib.println("Sending notif " + sending);
				if (sending != "") {
					client.socket.output.writeString(sending);
					client.socket.output.flush();
				}
			}
		}
		
	}
	
	function getMysqlConnexion():Connection
	{
		return Mysql.connect({ 
            host : "localhost",
            port : 3306,
            user : "ld31_user",
            pass : "QuqJzRTuS4M8urHu",
            socket : null,
            database : "ld31"
        });
	}
	
	function readImageFromRow(objectInRow:Dynamic):Array<UInt>
	{
		return (objectInRow != null) ? Unserializer.run(objectInRow.b + "") : null;
	}
	
	function getPlayersOnPlanet(planetid:Int, cnx:Connection, excludeByName:String):Array<Player> 
	{
		var players:Array<Player> = new Array<Player>();
		var rset = cnx.request("SELECT id, name, img, reputation, fuel, skills FROM player WHERE position_id = " + planetid + " AND online = 1 ORDER BY reputation");
		
		for ( row in rset ) {
			var p:Player = new Player();
			p.id = row.id;
			p.name = row.name;
			p.img = (row.img != null) ? Unserializer.run(row.img.b+"") : null;
			p.reputation = row.reputation;
			p.fuel = row.fuel;
			p.skills = row.skills;
			
			// adding to list
			if (p.name != excludeByName) {
				players.push(p);
			}
		}
		
		return players;
	}
	}
	
	/*
	static function main() {
		
		var server = new GameServer();
		Lib.println("Starting server...");
		server.run("localhost", 13337);
		
	}*/
	
package ld31.procedural;

/**
 * ...
 * @author Samuel Bouchet
 */
class NameGenerator
{

	public function new() 
	{
		
	}
	
	/**
	 * arranged from http://fantasynamegenerators.com/planet_names.php
	 */
	public static function generatePlanetName():String {
		var characters1 = ["b", "c", "d", "f", "g", "h", "i", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z"];
		var characters2 = ["a", "e", "o", "u"];
		var characters3 = ["br", "cr", "dr", "fr", "gr", "pr", "str", "tr", "bl", "cl", "fl", "gl", "pl", "sl", "sc", "sk", "sm", "sn", "sp", "st", "sw", "ch", "sh", "th", "wh"];
		var characters4 = ["ae", "ai", "ao", "au", "a", "ay", "ea", "ei", "eo", "eu", "e", "ey", "ua", "ue", "ui", "uo", "u", "uy", "ia", "ie", "iu", "io", "iy", "oa", "oe", "ou", "oi", "o", "oy"];
		var characters5 = ["turn", "ter", "nus", "rus", "tania", "hiri", "hines", "gawa", "nides", "carro", "rilia", "stea", "lia", "lea", "ria", "nov", "phus", "mia", "nerth", "wei", "ruta", "tov", "zuno", "vis", "lara", "nia", "liv", "tera", "gantu", "yama", "tune", "ter", "nus", "cury", "bos", "pra", "thea", "nope", "tis", "clite"];
		var characters6 = ["una", "ion", "iea", "iri", "illes", "ides", "agua", "olla", "inda", "eshan", "oria", "ilia", "erth", "arth", "orth", "oth", "illon", "ichi", "ov", "arvis", "ara", "ars", "yke", "yria", "onoe", "ippe", "osie", "one", "ore", "ade", "adus", "urn", "ypso", "ora", "iuq", "orix", "apus", "ion", "eon", "eron", "ao", "omia"];

		var random1 = Math.floor((Math.random() * characters1.length));
		var random2 = Math.floor((Math.random() * characters2.length));
		var random3 = Math.floor((Math.random() * characters3.length));
		var random4 = Math.floor((Math.random() * characters4.length));	
		var random5 = Math.floor((Math.random() * characters5.length));
		
		var random6 = Math.floor((Math.random() * characters2.length));
		var random7 = Math.floor((Math.random() * characters3.length));
		var random8 = Math.floor((Math.random() * characters4.length));
		var random9 = Math.floor((Math.random() * characters1.length));	
		var random10 = Math.floor((Math.random() * characters6.length));
		
		var random11 = Math.floor((Math.random() * characters3.length));
		var random12 = Math.floor((Math.random() * characters2.length));
		var random13 = Math.floor((Math.random() * characters1.length));
		var random14 = Math.floor((Math.random() * characters4.length));	
		var random15 = Math.floor((Math.random() * characters5.length));
		
		var random16 = Math.floor((Math.random() * characters4.length));
		var random17 = Math.floor((Math.random() * characters1.length));
		var random18 = Math.floor((Math.random() * characters2.length));
		var random19 = Math.floor((Math.random() * characters3.length));	
		var random20 = Math.floor((Math.random() * characters6.length));
		
		var random21 = Math.floor((Math.random() * characters1.length));
		var random22 = Math.floor((Math.random() * characters4.length));
		var random23 = Math.floor((Math.random() * characters3.length));
		var random24 = Math.floor((Math.random() * characters2.length));	
		var random25 = Math.floor((Math.random() * characters5.length));
		
		var random26 = Math.floor((Math.random() * characters2.length));
		var random27 = Math.floor((Math.random() * characters1.length));
		var random28 = Math.floor((Math.random() * characters4.length));
		var random29 = Math.floor((Math.random() * characters3.length));	
		var random30 = Math.floor((Math.random() * characters6.length));
		
		var random31 = Math.floor((Math.random() * characters3.length));
		var random32 = Math.floor((Math.random() * characters4.length));
		var random33 = Math.floor((Math.random() * characters1.length));
		var random34 = Math.floor((Math.random() * characters2.length));	
		var random35 = Math.floor((Math.random() * characters5.length));
		
		var random36 = Math.floor((Math.random() * characters4.length));
		var random37 = Math.floor((Math.random() * characters3.length));
		var random38 = Math.floor((Math.random() * characters2.length));
		var random39 = Math.floor((Math.random() * characters1.length));	
		var random40 = Math.floor((Math.random() * characters6.length));
		
		var random41 = Math.floor((Math.random() * characters3.length));
		var random42 = Math.floor((Math.random() * characters4.length));
		var random43 = Math.floor((Math.random() * characters1.length));
		var random44 = Math.floor((Math.random() * characters4.length));	
		var random45 = Math.floor((Math.random() * characters5.length));
		
		var random46 = Math.floor((Math.random() * characters4.length));
		var random47 = Math.floor((Math.random() * characters1.length));
		var random48 = Math.floor((Math.random() * characters4.length));
		var random49 = Math.floor((Math.random() * characters3.length));	
		var random50 = Math.floor((Math.random() * characters6.length));			
		
		var names = [
			characters1[random1] + characters2[random2] + characters5[random5],
			characters2[random6] + characters3[random7] + characters6[random10],
			characters3[random23] + characters4[random22] + characters2[random24] + characters5[random25],
			characters2[random26] + characters1[random27] + characters3[random29] + characters6[random30],
			characters3[random31] + characters4[random32] + characters2[random34] + characters5[random35],
			characters4[random46] + characters1[random47] + characters4[random48] + characters3[random49] + characters6[random50]
		];
		return firstUpper(names[Math.floor(Math.random() * names.length)]);
	}
	
	static function firstUpper(str:String) : String {
		var firstChar:String = str.substr(0, 1); 
		var restOfString:String = str.substr(1, str.length); 
		return firstChar.toUpperCase()+restOfString.toLowerCase(); 
	}
}
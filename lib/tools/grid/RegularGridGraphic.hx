package tools.grid;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.HXP;
import com.haxepunk.utils.Draw;
import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
	
/**
 * ...
 * @author Samuel Bouchet
 */
class RegularGridGraphic extends Image implements IGridGraphic
{
	private var _grid:Grid;
	private var _displayWidth:Int;
	private var _displayHeight:Int;
	private var _cellWidth:Int;
	private var _cellHeight:Int;
	private var _position:Point;
	private var cellBitmapData:BitmapData;
	private var buff:BitmapData;
	
	/**
	 * 
	 * @param	grid
	 * @param	displayWidth
	 * @param	displayHeight
	 */
	public function new(grid:Grid, displayWidth:Int, displayHeight:Int, cellBitmapData:BitmapData)
	{
		buff = new BitmapData(displayWidth, displayHeight, true, 0x00000000);
		super(buff, new Rectangle(0, 0, displayWidth + 1, displayHeight + 1));
		this.cellBitmapData = cellBitmapData;
		this.displayHeight = displayHeight;
		this.displayWidth = displayWidth;
		this.grid = grid;
		this.cellWidth = Math.round(displayWidth / grid.columnCount);
		this.cellHeight = Math.round(displayHeight / grid.lineCount);

		draw();
	}
	
	public function draw() {
		var cellTransformMatrix = new Matrix();
		cellTransformMatrix.scale(this.cellWidth / (cellBitmapData.width-1), this.cellHeight /( cellBitmapData.height-1));
		
		var pourc:Float;
		// trace toutes les lignes verticales
		var iWidth:Int = 0 ;
		for (x in 0...grid.columnCount) {
			for(y in 0...grid.lineCount) {
				cellTransformMatrix.tx = x * this.cellWidth;
				cellTransformMatrix.ty = y * this.cellHeight;
				#if flash
				_buffer.draw(cellBitmapData, cellTransformMatrix, null, null, null, true);
				#else
				buff.draw(cellBitmapData, cellTransformMatrix, null, null, null, true);
				#end
			}
		}
	}
	
	public function getCellPosition(cell:ICell):Point {
		var pourc:Float = cell.x / grid.columnCount;
		var amplitude:Float = ((cell.y+1) / grid.lineCount); // +1 to align on bottom left point of the cell
		
		var position = new Point();
		position.x = (cell.x) * cellWidth 				// cell position
					- amplitude + amplitude * 2 * pourc // perspective offset deformation
					+ (amplitude / grid.columnCount);	// bottom center
		position.y = (cell.y) * cellHeight;
		return position;
	}
	
	public function getCellAt(p:Point):ICell {
		
		if(grid!=null){
			for (x in 0...grid.columnCount) {
				for (y in 0...grid.lineCount) {
					var cell:Cell = grid.cells[x][y];
					var pourc:Float = cell.x / grid.columnCount;
					var rect = new Rectangle();
					rect.x = grid.x + (cell.x) * cellWidth; 		// cell position
					rect.y = grid.y + (cell.y) * cellHeight;
					rect.width = displayWidth / grid.columnCount;
					rect.height = displayHeight / grid.lineCount;
					if (rect.containsPoint(p)) {
						return cell;
					}
				}
			}
		}
		return null;
	}
	
	public function getWidth():Int
	{
		return Math.floor(width * HXP.screen.scale );
	}
	public function getHeight():Int
	{
		return Math.floor(height * HXP.screen.scale);
	}
	
	private function get_displayWidth():Int
	{
		return _displayWidth;
	}
	
	private function set_displayWidth(value:Int):Int
	{
		return _displayWidth = value;
	}
	
	public var displayWidth(get_displayWidth, set_displayWidth):Int;
	
	private function get_displayHeight():Int
	{
		return _displayHeight;
	}
	
	private function set_displayHeight(value:Int):Int
	{
		return _displayHeight = value;
	}
	
	public var displayHeight(get_displayHeight, set_displayHeight):Int;
	
	
	private function get_grid():Grid
	{
		return _grid;
	}
	
	private function set_grid(value:Grid):Grid
	{
		return _grid = value;
	}
	
	public var grid(get_grid, set_grid):Grid;
	
	function get_cellWidth():Int 
	{
		return _cellWidth;
	}
	function set_cellWidth(value:Int):Int 
	{
		return _cellWidth = value;
	}
	public var cellWidth(get_cellWidth, set_cellWidth):Int;
	
	function get_cellHeight():Int 
	{
		return _cellHeight;
	}
	function set_cellHeight(value:Int):Int 
	{
		return _cellHeight = value;
	}
	public var cellHeight(get_cellHeight, set_cellHeight):Int;

	
}
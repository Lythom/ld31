package ld31.model;

/**
 * @author Samuel Bouchet
 */

typedef TripChangeData =
{
	var from:Int;
	var destination:Int;
	var players:Array<String>; // null for trip cancel
}
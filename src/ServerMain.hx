package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import neko.vm.Thread;

/**
 * ...
 * @author Samuel Bouchet
 */

class ServerMain extends Sprite 
{
	var inited:Bool;
	var threadServer:GameServer;

	/* ENTRY POINT */
	
	function resize(e) 
	{
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		if (inited) return;
		inited = true;
		
		Thread.create(function() {
			threadServer = new GameServer();
			trace("Starting server...");
	#if debug
			threadServer.run("localhost", 13337);
	#else
			threadServer.run("samuel-bouchet.fr", 13337);
	#end
		});
		/*
		Thread.create(function() {
			policyServer = new PolicyServer();
			trace("Starting policy server...");
			policyServer.run("localhost", 843);
		});*/
		
		
		// Stage:
		// stage.stageWidth x stage.stageHeight @ stage.dpiScale
		
		// Assets:
		// nme.Assets.getBitmapData("img/assetname.jpg");
		
		
	}

	/* SETUP */

	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, added);
	}

	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new ServerMain());
	}
}

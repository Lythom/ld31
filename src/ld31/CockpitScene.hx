package ld31;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.Label;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.events.Event;
import haxe.Timer;
import ld31.entities.PlanetListView;
import ld31.entities.PlanetMainView;
import ld31.entities.PlayerListView;
import ld31.model.Planet;
import ld31.model.PlanetData;
import ld31.model.Player;
import ld31.model.PlayerChangeData;
import ld31.model.Trip;
import ld31.model.TripChangeData;
import ld31.net.ServerConnection;
import openfl.Lib;

/**
 * ...
 * @author Samuel Bouchet
 */
class CockpitScene extends Scene
{
	public static var instance:CockpitScene ;
	static public inline var GOAL:String = "(Tip) : Your goal is to help establishing communication with aliens.\nAny symbol you guessed grant you 2 reputation points.\nAny of your symbol used by another player grant you 1 point.";
	
	var connected:Bool = false;
	var connectedLbl:Label;
	var connectedColor = 0x37E363;
	var notconnectedColor = 0xDD893E;
	var bg:Entity;
	
	var RETRY_TIMEOUT_MAX:Int = 1000;
	var retry_timeout:Int = 0;
	
	var thisplanet:Planet;
	var thisplayer:Player;
	var currentPlayerView:PlayerListView;
	var currentPlanetMainView:PlanetMainView;
	var otherPlayerView:Array<PlayerListView>;
	var planetListViews:Array<PlanetListView>;
	var columntitles:Planet;
	var goButtons:Array<Button> = new Array<Button>();
	
	var symbolButtons:Array<Button> = new Array<Button>();
	
	var currentTrip:Trip;
	
	var console:Label;
	var energy:Label;

	var energyTimer:Timer;
	var planets:Array<Planet>;
	var thisplanetbutton:Button;
	var thisplayerbutton:Button;
	var thispopbutton:Button;
	var players:Array<Player>;
	var lastTriedLetter:String;
	
	var currentScreen:String = "";
	var tripCD:Int = -10;

	public function new() 
	{
		super();
		instance = this;
		
		// init background
		bg = new Entity();
		bg.graphic = new Image("gfx/bg.png");
		
		// connection labels
		connectedLbl = new Label("Connecting...", 994, 24);
		connectedLbl.color = notconnectedColor;

		// init player list
		otherPlayerView = new Array<PlayerListView>();
		currentPlayerView = new PlayerListView(175, 185, null);
		currentPlayerView.isMainPlayer = true;
		for (i in 0...8) {
			otherPlayerView.push(new PlayerListView(373 + 116 * i, 295, reportShip));
		}
		
		// init world visuals
		currentPlanetMainView = new PlanetMainView(67,105);
		
		// init neairbour worlds list
		planetListViews = new Array<PlanetListView>();
		for (i in 0...9) {
			planetListViews.push(new PlanetListView(80, 500 + 32 * i));
		}
		
		columntitles = new Planet();
		columntitles.distance = 99999999;
		columntitles.posx = 99999999;
		columntitles.posy = 99999999;
		columntitles.name = "Planet Name";
		columntitles.popphrasecurrent = "";
		
		// console
		console = new Label("Please wait for the server to answer...\n"+GOAL, 572, 47, 618, 277);
		console.color = 0xFFFFFF;
		console.multiline = true;
		
		energy = new Label("0%", 206, 440);
		energy.color = 0xFFFFFF;
		energy.size = 64;
		energyTimer = new Timer(1000);
		energyTimer.run = function() {
			if (thisplayer != null) {
				energy.text = thisplayer.fuel + "%";
				thisplayer.fuel = Std.int(Math.min(100, thisplayer.fuel+1));
			}
			if (tripCD > -2) {
				console.text += "," + tripCD;
				tripCD--;
			} else if (tripCD != -10) {
				consoleReset("Engine failure, cancelling trip. Please retry.");
				tripCD = -10;
				for (gb in goButtons) {
					add(gb);
				}
			}
		};
		
		// interaction buttons
		thisplanetbutton = new Button("", 28, 51, 160, 200);
		thisplanetbutton.addEventListener(Button.CLICKED, thisplanetbuttonclick);
		
		thisplayerbutton = new Button("", 157, 138, 160, 200);
		thisplayerbutton.addEventListener(Button.CLICKED, thisplayerbuttonclick);
		
		thispopbutton = new Button("", 276, 48, 160, 200);
		thispopbutton.addEventListener(Button.CLICKED, thispopbuttonclick);
		
		for (i in 0...26) {
			var b = new Button(".", 396 + 28 * i, 450, 27, 40);
			b.color = 0x0094FF;
			b.size = 32;
			b.addEventListener(Button.CLICKED, clickSymbol);
			symbolButtons.push(b);
		}
	}
	
	override public function begin()
	{
		super.begin();
		add(bg);
		bg.layer = 1000;
		// connect to server
		add(connectedLbl);
		add(currentPlanetMainView);
		add(currentPlayerView);

		for (playerview in otherPlayerView) {
			add(playerview);
		}

		for (planetView in planetListViews) {
			add(planetView);
		}
		
		for (s in symbolButtons) {
			add(s);
		}
		
		add(console);
		add(energy);
		
		ServerConnection.initNetwork();
		retry_timeout = RETRY_TIMEOUT_MAX;
		
		add(thisplanetbutton);
		add(thisplayerbutton);
		add(thispopbutton);
		
	}
	
	override public function update()
	{
		// try to reconnect to server if not connected
		if (!connected) {
			if (retry_timeout < 0) {
				consoleAppend( "Trying to join the server...");
				ServerConnection.initNetwork();
				retry_timeout = RETRY_TIMEOUT_MAX;
			} else {
				retry_timeout -- ;
			}
		} else {
			/*
			if (Input.pressed(Key.SPACE)) {
				ServerConnection.sendWelcome();
			}
			if (Input.pressed(Key.ESCAPE)) {
				ServerConnection.initNetwork();
				retry_timeout = RETRY_TIMEOUT_MAX;
			}
			*/
		}
		
		super.update();
	}
	
	override public function end()
	{
		super.end();
		removeAll();
	}
	
		
	private function thispopbuttonclick(?e:Event):Void 
	{
		if (thisplanet == null) return;
		currentScreen = "pop";
		consoleReset("Analyzing race...");
		consoleAppend("Relation between '"+thisplayer.name+"' and '"+thisplanet.name+"' : \n"+getDiplomaticState(thisplanet.triescount));
		console.text += "\n> Translation to decode : \n" + thisplanet.popphrasecurrent + "\n\n";
		if (thisplanet.triescount < 5) {
			console.text += "<Select a Symbol at the top of the bottom screen to have a guess>";
		}
	}
	
	function getDiplomaticState(triescount:Int):String
	{
		return switch (triescount) 
		{
			case 0: "Curious : Go on and try to make them your friend !";
			case 1: "Listening : Only one mistake, you can keep working.";
			case 2: "Bored : Ok there's still a few tries left.";
			case 3: "Uneasy : That's getting close to an end.";
			case 4: "Nervous : One more mistake and I'm Out There.";
			default : "Agressive : It's over, I won't get anything else from them.";
		};
	}
	
	private function thisplanetbuttonclick(e:Event):Void 
	{
		if (thisplanet == null) return;
		currentScreen = "planet";
		consoleReset("Analyzing planet...");
		consoleAppend("Name : " + thisplanet.name);
		consoleAppend("X Coord : " + thisplanet.posx);
		consoleAppend("Y Coord : " + thisplanet.posy);
		consoleAppend("Translation progress : " + (Std.string(thisplanet.getProgress()) + "%"));
		consoleAppend("Active players currently there : " + thisplanet.playercount);
		consoleAppend("(Tip) : You get a random new symbol each time you arrive on a planet for the first time. But if you already have it, you get nothing !");
	}
	
	private function thisplayerbuttonclick(e:Event):Void 
	{
		if (thisplayer == null) return;
		currentScreen = "player";
		reportShip(thisplayer);
		consoleAppend(GOAL);
		consoleAppend("(Tip) : Players automatically shares symbols (blue = yours, orange = borrowed), and power cost if they travel together ! Watch out for players trips to move faster or further.");
	}
	
	private function reportShip(player:Player) {
		if (player != null) {
			currentScreen = "ship";
			consoleReset("Analyzing Ship...");
			consoleAppend("Captain : " + player.name);
			consoleAppend("Reputation : " + player.reputation);
			consoleAppend("Discovered symbols ("+Std.string(26 - Math.max(0,player.skills.split(".").length-1))+") :\n" + player.skills);
			
		} else {
			consoleReset("No ship there...");
			consoleAppend("Connected players in the universe : "+thisplanet.playercount);
			consoleAppend("(Tip) : Invite friends to share symbols and travel costs !\nNo friend around ? Feel free to open a second tab to play 2 ships at the same time and discover multiplay features !");
		}
	}
	
	
	public function welcome(data:PlanetData) 
	{
		tripCD = -10;
		connected = true;
		connectedLbl.color = connectedColor;
		connectedLbl.text = "Connected to server ☃";
		
		consoleReset("Ship Commands are operationnal.\n> " + GOAL);
		
		// with the welcome comes the 1st data of the worlds around
		players = data.players;
		planets = data.planets;
		
		// update planet data
		thisplanet = data.thisplanet;
		currentPlanetMainView.viewPlanet(thisplanet);
		
		// update player data
		thisplayer = data.thisplayer;
		currentPlayerView.viewPlayer(thisplayer);
		refreshSymboles(thisplayer);
		
		// update player list
		var i = 0;
		for (playerview in otherPlayerView) {
			if (players != null && players.length > i) {
				playerview.viewPlayer(players[i]);
			} else {
				playerview.viewPlayer(null);
			}
			i++;
		}
		
		// update planet list
		i = 0;
		for (planetView in planetListViews) {
			if (i == 0) {
				planetView.viewPlanet(columntitles);
			} else {
				if (planets != null && planets.length > i) {
					planetView.viewPlanet(planets[i]);
					var goButton = new Button("Start the trip", Math.round(planetView.planetdata.x + planetView.planetdata.width + 5), planetView.planetdata.y - 5, 0, 32);
					goButton.color = 0x0094FF;
					goButton.label.valign = VerticalAlign.TOP;
					goButton.label.bitmapBufferMargin = -2;
					var planetid = planets[i].id;
					goButton.addEventListener(Button.CLICKED, function(e) {
						startTrip(planetid);
					});
					goButtons.push(goButton);
					add(goButton);
				} else {
					planetView.viewPlanet(null);
				}
			}
			i++;
		}

		consoleAppend( "Planet '"+thisplanet.name+"' scanned. Data have been updated.");
	}
	
	function refreshSymboles(thisplayer:Player) 
	{
		// cumulated symbols
		var playerskill:String = thisplayer.skills;
		var cumulatedskill:String = thisplayer.skills;
		for (i in 0...cumulatedskill.length) {
			if (cumulatedskill.charAt(i) == ".") {
				for (p in players) {
					if (p.skills.charAt(i) != ".") {
						cumulatedskill = cumulatedskill.substring(0, i) + p.skills.charAt(i) + cumulatedskill.substring(i + 1);
					}
				}
			}
		}
		
		for (i in 0...playerskill.length) {
			if (symbolButtons.length > i) {
				var char = cumulatedskill.charAt(i);
				if (thisplanet.triescount >= 5) {
					symbolButtons[i].enabled = false;
					
				} else if (char == "."){
					symbolButtons[i].enabled = false;
					
				} else {
					symbolButtons[i].enabled = true;
					if (playerskill.charAt(i) != ".") {
						// player skill
						symbolButtons[i].color = 0x0094FF;
					} else {
						// cumulated skill
						symbolButtons[i].color = 0xFAAA05;
					}
				}
				symbolButtons[i].text = cumulatedskill.charAt(i);
				
			}
		}
	}
	
	function clickSymbol(e:ControlEvent) {
		if (Std.is(e.control, Button)) {
			var b = cast(e.control, Button);
			if (b.text != ".") {
				ServerConnection.sendTryLetter(b.text);
				lastTriedLetter = b.text;
			}
		}
		refreshSymboles(thisplayer);
	}
	
	public function startTrip(planetid:Int) {
		for (gb in goButtons) {
			remove(gb);
		}
		
		// TODO : display cancel option.
		ServerConnection.sendStartTrip(planetid);
		currentTrip = {
			from: thisplanet.id,
			to: planetid,
			with : [thisplayer.id],
			starttime: Timer.stamp(),
			cost: planets.filter(function(planet) { return planet.id == planetid; } )[0].distance
		};
	}
	
	public function cancelTrip(planetid:Int) {
		consoleAppend( "Trip cancellation required...");
		ServerConnection.sendCancelTrip(planetid);
	}
	
	public function goto(planetdata:PlanetData) {
		// empty the list of buttons
		goButtons.splice(0, goButtons.length);
		welcome(planetdata);
	}
	
	public function disconnect() 
	{
		consoleAppend( "Connection lost, auto reconnect in a few seconds...");
		connected = false;
		connectedLbl.color = notconnectedColor;
		connectedLbl.text = "Server is offline =(";
	}
	
	public function startTripConfirmation(data:String) 
	{
		if (data == "new") {
			currentScreen = "trip";
			consoleReset( "Calling for player to join. Start in 10 sec...");
			tripCD = 10;

		} else if (data == "join") {
			consoleAppend( "Joining the trip ! Starting soon...");
			tripCD = 8;
		} else {
			consoleAppend( "Trip group couldn't be created.");
		}
	}
	
	public function consoleAppend(string:String) 
	{
		var text:String = "";
		var lines = console.text.split("\n");
		while(lines.remove("")){}
		lines.push("> " + string);
		for (i in lines.length - 8...lines.length) {
			if (i >= 0 && i < lines.length) {
				text += lines[i] + "\n";
			}
		}
		console.text = text;
	}
	
	public function consoleReset(string:String) 
	{
		console.text = string;
	}
	
	public function cancelTripConfirmation(data:Bool) 
	{
		if (data) {
			consoleAppend( "Trip canceled : the group doesn't have enough fuel");
		} else {
			consoleAppend( "Trip cancellation failure, the group already left.");
		}
		for (gb in goButtons) {
			add(gb);
		}
	}
	
	public function playerChange(data:PlayerChangeData) 
	{
		
		var newPlayerList:Array<Player> = otherPlayerView.map(function(plv) { return plv.player; } );
		if (data.arrived) {
			for (p in data.playernames) {
				if (!Lambda.exists(newPlayerList, function(p2) { return p2 != null && p.name == p2.name; } )) {
					newPlayerList.push(p);
				}
			}

		} else if (data.left) {
			for (p in data.playernames) {
			if (p != null) {
				for (p2 in newPlayerList.copy()) {
					if (p2 != null && p.name == p2.name) {
						newPlayerList.remove(p2);
					}
				}
			}
			}
		}
		newPlayerList = newPlayerList.filter(function(p) { return p != null && thisplayer != null && p.name != thisplayer.name; } );

		var i = 0;
		for (playerview in otherPlayerView) {
			if (newPlayerList != null && newPlayerList.length > i) {
				playerview.viewPlayer(newPlayerList[i]);
			} else {
				playerview.viewPlayer(null);
			}
			i++;
		}
		
		players = newPlayerList;
		
		// refresh available letters
		refreshSymboles(thisplayer);
	}
	
	public function tripChange(data:TripChangeData) 
	{
		// exception, if dest = -1 then it's a passphrase update
		if (data.destination == -1) {
			passphraseUpdate(data.players[0]);
			return;
		}
		
		if (currentTrip != null && currentTrip.from == data.from && currentTrip.to == data.destination && data.players[0] != thisplayer.name) {
			//  change to trip list instead od show info in console
			consoleAppend("A player join your trip : " + data.players.join(", "));
			
		} else {
			// this change affect another trip, display it on screen
			var destination = planets.filter(function(planet) { return planet.id == data.destination; } );
			if (destination.length > 0) {
				// TODO : display on screen instead of text : change button label
				consoleAppend(data.players[0] + " started a trip to " + destination[0].name +". Click 'Start the trip' on this planet to join !");
			}
		}

	}
	
	function passphraseUpdate(response:String) 
	{
		if (response != null) {
			// YAY !
			for (i in 0...response.length) {
				if (thisplanet.popphrasecurrent.charAt(i) != response.charAt(i)) {
					var letterChange = response.charAt(i);
					if (thisplayer.skills.indexOf(letterChange.toUpperCase()) > -1) {
						thisplayer.reputation ++;
					}
				}
			}
			thisplanet.popphrasecurrent = response;
			if (currentScreen == "pop") {
				thispopbuttonclick();
			}
		}
	}
	
	public function letterResult(data:String) 
	{
		currentScreen = "result";
		if (data != null) {
			thisplanet.popphrasecurrent = data;
			thisplayer.reputation += 2;
			for (p in players) {
				if (p.skills.indexOf(lastTriedLetter) > -1) {
					p.reputation ++;
				}
			}
			consoleReset("You tried to communicate... And succeed !");
			consoleAppend("Reputation : " + thisplayer.reputation);
			consoleAppend("Translation progress : " + (Std.string(thisplanet.getProgress()) + "%"));
			console.text += "\n> Translation to decode : \n" + thisplanet.popphrasecurrent + "\n\n";
			console.text += "<Select a Symbol at the top of the bottom screen to have a guess>";
		} else {
			thisplanet.triescount ++;
			consoleReset("You tried to communicate... And failed !");
			consoleAppend("Relation between '" + thisplayer.name+"' and '" + thisplanet.name+"' : \n" + getDiplomaticState(thisplanet.triescount));
			console.text += "\n> Translation to decode : \n" + thisplanet.popphrasecurrent + "\n\n";
			console.text += "<Select a Symbol at the top of the bottom screen to have a guess>";
		}
		
		
	}
	
}
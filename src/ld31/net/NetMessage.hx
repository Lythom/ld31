package ld31.net ;

enum NetMessageType {
	Welcome;	// client send name:String // server answer with PlanetData
	StartTrip;  // client send planet_id   // server answer with true
	CancelTrip; // client send void        // server answer with true if the trip is cancelled, false if the trip has departure.
	Goto;       //                         // server indicate with PlanetData
	PlayerChange; //                       // server indicate with PlayerChangeData
	TripChange; //                         // server indicates new trip states
	TryLetter;  // client send char        // server indicated good (new phrase) or bad (null)
}

/**
 * ...
 * @author Samuel Bouchet
 */
class NetMessage
{
	public var type:NetMessageType;
	public var data:Dynamic;
		
	
	public function new(type:NetMessageType, data:Dynamic = null) 
	{
		this.data = data;
		this.type = type;
	}
	
}
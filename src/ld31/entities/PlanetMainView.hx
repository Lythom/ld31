package ld31.entities;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.Mask;
import ld31.model.Planet;
import openfl.display.BitmapData;
import openfl.filters.DropShadowFilter;
import tools.image.Scale3XUpscaler;

/**
 * ...
 * @author Samuel Bouchet
 */
class PlanetMainView extends Control
{
	
	var planetimg:Control;
	var planetname:Label;
	
	var popimg:Control;
	var popname:Label;

	public function new(x:Float=0, y:Float=0) 
	{
		super(x, y);
		
		planetimg = new Control(0,0);
		addControl(planetimg);
		
		planetname = new Label("", 0, -25);
		planetname.color = 0xC2D2FE;
		planetname.size = 32;
		planetname.textField.filters = [new DropShadowFilter(2,45,0x0063C6,1,0,0,1,2,false),new DropShadowFilter(2,-135,0x0063C6,1,0,0,1,1,false)];
		addControl(planetname);
		
		popimg = new Control(248, 0);
		addControl(popimg);
		
		popname = new Label("", 0, -25);
		popname.color = 0xC2D2FE;
		popname.size = 32;
		popname.textField.filters = [new DropShadowFilter(2,45,0x0063C6,1,0,0,1,2,false),new DropShadowFilter(2,-135,0x0063C6,1,0,0,1,1,false)];
		addControl(popname);
	}
	
	public function viewPlanet(planet:Planet) {
		
		if (planet != null) {
			
			// planet
			var planetbitmapData:BitmapData = Scale3XUpscaler.upscale(Scale3XUpscaler.upscale(planet.getImage()));
			planetimg.graphic = new Image(planetbitmapData);
			planetname.text = planet.name;
			planetname.localX = planetbitmapData.width/2 - planetname.width / 2;
			
			// pop
			var popbitmapData:BitmapData = Scale3XUpscaler.upscale(Scale3XUpscaler.upscale(planet.getPopImage()));
			popimg.graphic = new Image(popbitmapData);
			popname.text = planet.name + " inhabitant";
			popname.localX = popimg.localX +  popbitmapData.width / 2 - popname.width / 2;
			
			
			
		} else {
			planetimg.graphic = null;
			planetname.text = "";
			
			popimg.graphic = null;
			popname.text = "";
		}
	}
	
}
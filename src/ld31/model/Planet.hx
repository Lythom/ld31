package ld31.model ;
import openfl.display.BitmapData;
import openfl.utils.ByteArray;
import openfl.Vector;

using StringTools;

/**
 * ...
 * @author Samuel Bouchet
 */
class Planet
{
	public var id:Int;
	public var name:String;
	public var posx:Int;
	public var posy:Int;
	public var img:Array<UInt>;
	public var popimg:Array<UInt>;
	public var popphrasecurrent:String;
	public var playercount:Int;

	// for current player
	public var triescount:Int;
	
	// for current world
	public var distance:Int;
	
	
	public function new() 
	{
	}
	
	public function getProgress():Float {
		var missing:Float = 100 * (Math.max(0,popphrasecurrent.split(".").length - 1)) / popphrasecurrent.replace(" ","").length;
		return Math.round((100-missing) * 100) / 100;
	}
	
	public function getImage():BitmapData {
		if (img == null) return null;
		var image:BitmapData = new BitmapData(10, 10, true, 0x00000000);
		image.setVector(image.rect, Vector.fromArray(img));
		return image;
	}
	
	public function getPopImage():BitmapData {
		if (popimg == null) return null;
		var image:BitmapData = new BitmapData(8, 11, true, 0x00000000);
		image.setVector(image.rect, Vector.fromArray(popimg));
		return image;
	}
	
}
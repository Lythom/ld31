package ld31.model ;

import haxe.io.BytesInput;
import haxe.io.BytesOutput;
import openfl.display.BitmapData;
import openfl.utils.ByteArray;
import openfl.Vector;

/**
 * ...
 * @author Samuel Bouchet
 */
class Player
{
	public var name:String;
	public var img:Array<UInt>;
	public var reputation:Int;
	public var fuel:Int;
	public var skills:String;
	public var id:Int;
	
	public function new() 
	{
	}
	
	public function getImage():BitmapData {
		if (img == null) return null;
		var image:BitmapData = new BitmapData(7, 7, true, 0x00000000);
		image.setVector(image.rect, Vector.fromArray(img));
		return image;
	}
	
}
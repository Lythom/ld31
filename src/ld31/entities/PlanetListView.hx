package ld31.entities;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.Mask;
import ld31.model.Planet;
import openfl.Assets;
import openfl.display.BitmapData;
import tools.image.Scale3XUpscaler;

using StringTools;

/**
 * ...
 * @author Samuel Bouchet
 */
class PlanetListView extends Control
{
	
	var planetimg:Control;
	public var planetdata:Label;

	public function new(x:Float=0, y:Float=0) 
	{
		super(x, y);
		
		planetimg = new Control(0,0);
		addControl(planetimg);
		
		planetdata = new Label("", 50, 0);
		planetdata.color = 0xBACDFE;
		planetdata.size = 16;
		planetdata.font = Assets.getFont("font/monaco.ttf").fontName;
		addControl(planetdata);
	}
	
	public function viewPlanet(planet:Planet) {
		
		if (planet != null) {
			
			// planet
			if (planet.getImage() != null) {
				var planetbitmapData:BitmapData = Scale3XUpscaler.upscale(planet.getImage());
				planetimg.graphic = new Image(planetbitmapData);
			} else {
				planetimg.graphic = null;
			}
			
			var data:String = "";
			data += planet.name.rpad(" ", 30) + "| ";
			if (planet.distance != 99999999) {
				data += Std.string(planet.distance).rpad(" ", 6) + "| ";
			} else {
				data += "Cost".rpad(" ", 6) + "| ";
			}
			if (planet.popphrasecurrent != "") {
				data += (Std.string(planet.getProgress()) + "%").rpad(" ", 9) + "| ";
			} else {
				data += "Progress".rpad(" ", 9) + "| ";
			}
			if (planet.posx != 99999999) {
				data += Std.string(planet.posx).rpad(" ", 6) + "| ";
			} else {
				data += "X".rpad(" ", 6) + "| ";
			}
			if (planet.posy != 99999999) {
				data += Std.string(planet.posy).rpad(" ", 6) + "| ";
			} else {
				data += "Y".rpad(" ", 6) + "| ";
			}
			planetdata.text = data;
			
		} else {
			planetimg.graphic = null;
			planetdata.text = "";
		}
	}
	
}
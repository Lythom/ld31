package ld31.model;

/**
 * @author Samuel Bouchet
 */

typedef PlayerChangeData =
{
	var playernames:Array<Player>;	// player name.
	var arrived:Bool;	// bool
	var left:Bool;      // bool
}
package ld31.procedural;
import openfl.utils.ByteArray;
import psg.Mask;
import psg.Sprite;
import openfl.display.BitmapData;

/**
 * ...
 * @author Samuel Bouchet
 */
class SpriteGenerator
{

	public static function generateSprite(mask:Mask, times:Int = 1):BitmapData
	{
		var newsprite:Sprite = new Sprite(mask, true, 0.45, 0.4, 0.2, 0.3);
		return newsprite.bitmap.bitmapData;
	}
	
}